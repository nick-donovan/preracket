#lang racket

; utility functions used by multiple files.

(provide (all-defined-out))

; return true if c is an operator
(define operators '(#\+ #\* #\/))
(define (operator? c)
  (member c operators))

; return true if c is a '-' 
(define (negation? c)
  (char=? c #\-))

; return true if c is a history request
(define (history? c)
  (char=? c #\$))

; retrieve the item at index idx in lst
(define (nth lst idx)
  (define (nth-helper lst idx curr)
    (if (= curr idx)
        (car lst)
        (nth-helper (cdr lst) idx (+ 1 curr))))
  
  ; verify idx is valid, if not return #f
  (if (or (> 0 idx) (>= idx (length lst)))
      #f
      (nth-helper lst idx 0)))
