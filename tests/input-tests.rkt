#lang racket

; test cases using the parse-input function in input-tests.rkt

(require "../input.rkt" "test-utility.rkt")

(assert-equals '(1) (parse-input "1"))
(assert-equals '(2) (parse-input "2"))
(assert-equals '(3) (parse-input "3"))
(assert-equals '(#\* #\* #\$ 3 #\$ 2 #\$ 1) (parse-input "**$3$2$1"))
(assert-equals '(#\/ #\+ #\+ #\+ 80 100 90 79 4) (parse-input "/+++80 100 90 79 4"))
(assert-equals '(#\/ #\+ #\+ #\/ 85 90 #\/ 78 90 #\/ 84 90 3) (parse-input "/++/85 90 /78 90 /84 90 3"))
(assert-equals '(#\* 100 #\$ 2) (parse-input "*100$2"))
(assert-equals '(#\+ #\/ #\* #\$ 1 40 100 #\/ #\* #\$ 3 60 100) (parse-input "+/*$1 40 100 /*$3 60 100"))
(assert-equals '(#\+ #\$ 1 #\- 1) (parse-input "+$1-1"))
(assert-equals '(#\+ #\$ 3 #\- 1) (parse-input "+$3-1"))
(assert-equals '(#\* #\$ 3 #\$ 3) (parse-input "*$3$3"))
(assert-equals '(#\+ #\$ 5 #\- 1) (parse-input "+$5-1"))
(assert-equals '(#\+ #\$ 5 #\- 1) (parse-input "+$5-1"))
(assert-equals '(#\* #\$ 5 #\$ 5) (parse-input "*$5$5"))
(assert-equals '(#\+ #\$ 7 #\- 1) (parse-input "+$7-1"))
(assert-equals '(#\* #\/ 17 100 215) (parse-input "*/17 100 215"))
(assert-equals '(#\+ 215 #\- #\$ 1) (parse-input "+215-$1"))
(assert-equals '(#\* #\/ 40 100 #\$ 2) (parse-input "*/40 100 $2"))
(assert-equals '(#\+ #\$ 2 #\- #\$ 3) (parse-input "+$2-$3"))
(assert-equals '(#\/ 1 0) (parse-input "/1 0"))
(assert-equals '(0) (parse-input "0"))
(assert-equals '(#\/ 14 #\$ 1) (parse-input "/14 $1"))
(assert-equals '(#\+  10) (parse-input "+10"))
(assert-equals '(#\+ #\+ 10 11) (parse-input "++10 11"))

(assert-throws parse-input "0.15")
(assert-throws parse-input "+5 .6")
(assert-throws parse-input ".6")

(assert-equals '(1 1) (parse-input "1 1"))
(assert-equals '(5 #\/ 6) (parse-input "5/6"))
(assert-equals "quit" (parse-input "quit"))
