# PreRacket
This was my first exploration into functional programming languages and Racket. PreRacket is an expression calculator that evaluates expressions in prefix notation.

## Usage
After launching the program using `racket main.rkt`. As you enter prefix expressions, a history will be built. The history is a simple list of values associated with an ID which can be used for future calculations.

## Features
- Expression Evaluation - Supports prefix notation.
- Error Handling - Specific error messages for clear handling
- History - Maintains a history of values and IDs for convenience

## Expressions
Binary Operators:
- `+`
- `*`
- `/`

Unary Operator:
- `-` - Negates the value of an expression.

Quit:
- `quit` - Exits the program
## Example
```
> racket main.rkt
Enter Expression: + 3 2
1: 5
Enter Expression: * $1 5
2: 25
Enter Expression: - $2
3: -25
Enter Expression: quit
```

## Files
- `main.rkt` - The main driver of the calculator program
- `input.rkt` - Parses the input string into a list for calculations
- `calculate.rkt` - Performs the calculation using the parsed input
- `utility.rkt` - Provides utility functions used by multiple files

## Tests
- PreRacket also has several test modules which can be ran from the root project directory with the commands:
	- `racket tests/input-tests.rkt` - Tests the input parser
	- `racket tests/calculate-test.rkt` - Tests the prefix calculations
- `#t` indicates the test has passed successfully.
